tool
extends Node2D

export(String) var title setget set_title, get_title
export(NodePath) var title_node_path
onready var title_node = get_node(title_node_path)

func set_title(value: String) -> void:
	if not is_inside_tree(): yield(self, 'ready')
	title_node.text = value

func get_title() -> String:
	if not title_node: return ''
	return title_node.text
