# How to Set Children's Properties in `tool` nodes?

TL,DR:

```gdscript
tool
extends Node2D

export(String) var title setget set_title, get_title
onready var title_node = $path/to/label

func set_title(value: String) -> void:
	if not is_inside_tree(): yield(self, 'ready')
	title_node.text = value

func get_title() -> String:
	if not title_node: return ''
	return title_node.text
```

This project represents a sample of a working application

## License

Free domain

## Icon License

Godot Logo (C) Andrea Calabró Distributed under the terms of the Creative Commons Attribution License version 3.0 (CC-BY 3.0) https://creativecommons.org/licenses/by/3.0/legalcode.
